from random import randint, choice

def main():
    print_intro()
    high_score = get_high_score()
    wants_to_play = True
    while wants_to_play:
        board = get_new_board() #complete
        score = 0
        while not board_is_full(board): #complete
            add_random_number(board) #complete
            print_board(board, score, high_score) #complete
            move = get_valid_move() #complete
            update_board(board, move)
            if score > high_score:
                high_score = score
        print_result() #Complete
        wants_to_play = get_valid_answer() #Complete
    print_outro() #Complete


def print_intro():
    print("          Welcome, BRO")

def get_high_score():
    pass

def get_new_board():
    board = [0 for _ in range(16)]
    add_random_number(board)
    return board

def board_is_full(board):
    for cell in board:
        if cell == 0:
            return False
    return True

def add_random_number(board):
    number = 4 if randint(1,10) == 1 else 2
    empty_cells = []
    for i in range(len(board)):
        if board[i] == 0:
            empty_cells.append(i)
    board[choice(empty_cells)] = number

def print_board(board, score, high_score):
    print_scores(score, high_score)
    print("+--------+--------+--------+--------+")
    for y in range(4):
        for i in range(3):
            print("|", end="")
            for x in range(4):
                index = y * 4 + x
                if i != 1 or board[index] == 0:
                    print("        |", end="")
                else:
                    space = (8 - len(str(board[index]))) // 2
                    print(" " * (space + len(str(board[index])) % 2), end="")
                    print(board[index], end="")
                    print(" " * space, end="|")

            print()
        print("+--------+--------+--------+--------+")

def print_scores(score, high_score):
    print(f"  SCORE:   {score}{' ' * (9 - len(str(score)))}HI:   {high_score}")

def get_valid_move():
    move = input("Введите W, A, S, D для выбора направления или R для выхода").lower()
    while move not in tuple("wasdr"):
        move = input("Введите W, A, S, D для выбора направления или R для выхода").lower()
    return move

def update_board():
    pass

def print_result(board, score, high_score):
    if has_won(board):
        print("YOU WIN!")
    else:
        print("YOU LOSE!")
    print(f"YOUR RESULT {score}")
    if score == high_score:
        print("THIS IS NEW RECORD!")

def has_won(board):
    for cell in board:
        if cell >= 2048:
            return True
    return False

def get_valid_answer():
    answer = input("Может повторим? (y/n)").lower()
    while move not in ("y", "n"):
        move = input("Необходимо ввести y или n").lower()
    return False if answer == "n" else True

def print_outro():
    print("Ты красаучик!")
    input("Ткни ENTER")

main()