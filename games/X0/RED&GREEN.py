import pygame
import time




def main():
    pygame.init()
    screen = pygame.display.set_mode((400, 420))
    filled = [0 for i in range(9)]
    font = pygame.font.Font(None, 32)
    current_player = "RED"
    done = False

    while not done:
        for event in pygame.event.get():

            screen.fill((0, 0, 0))
            text = CPlayer(current_player, font)
            screen.blit(text, (100, 10))
            sprites = print_board(screen, filled)
            filled, current_player = move(event, sprites, filled, current_player)
            done = whowon(filled, screen)

            if done == True:
                win = font.render("ПОБЕДИТЕЛЬ!", True, (255, 255, 255))
                screen.blit(win, (120, 200))
                pygame.display.flip()
                time.sleep(2)

            if 0 in filled:
                break
            elif done:
                break
            else:
                screen.fill((255, 255, 255))
                nowin = font.render("НИЧЬЯ!", True, (0, 0, 0))
                screen.blit(nowin, (150, 200))
                pygame.display.flip()
                time.sleep(2)
                return

            if event.type == pygame.QUIT:
                done = True

        pygame.display.flip()

def CPlayer(current_player, font):
    color = (128, 0, 0)
    if current_player == "RED":
        color = (128, 0, 0)
    else:
        color = (0, 128, 0)
    text = font.render(f"Сейчас ходит {current_player}", True, color)
    return text

def print_board(screen, filled):
    sprites = []
    x = 20
    y = 40
    sizeX = 110
    sizeY = 110
    color = (255,255,255)
    c = 0
    for a in range(3):
        for i in range(3):
            rect = pygame.Rect(x, y, sizeX, sizeY)

            if filled[c] == "RED":
                color = (128, 0, 0)
            elif filled[c] == "GREEN":
                color = (0, 128, 0)
            else:
                color = (255,255,255)

            pygame.draw.rect(screen, color, pygame.Rect(x, y, sizeX, sizeY))
            x += sizeX + 15
            sprites.append(rect)
            c += 1
        x = 20
        y += sizeY + 15

    return sprites

def move(event, sprites, filled, current_player):
    if event.type == pygame.MOUSEBUTTONUP:
        pos = pygame.mouse.get_pos()
        x = 0
        for i in sprites:
            if i.collidepoint(pos):
                if current_player == "RED":
                    if filled[x] == "RED" or filled[x] == "GREEN":
                        pass
                    else:
                        filled[x] = "RED"
                        current_player = "GREEN"
                else:
                    if filled[x] == "RED" or filled[x] == "GREEN":
                        pass
                    else:
                        filled[x] = "GREEN"
                        current_player = "RED"
            x +=1
    return filled, current_player

def whowon(filled, screen):
    winner = None
    win_combination = (
        (0, 1, 2), (3, 4, 5), (6, 7, 8),
        (0, 3, 6), (1, 4, 7), (2, 5, 8),
        (0, 4, 8), (2, 4, 6)
    )
    for pos in win_combination:
        if filled[pos[0]] == "RED" and filled[pos[1]] == "RED" and filled[pos[2]] == "RED":
            winner = "RED"
        elif filled[pos[0]] == "GREEN" and filled[pos[1]] == "GREEN" and filled[pos[2]] == "GREEN":
            winner = "GREEN"

    if winner == "RED":
        screen.fill((255, 0, 0))
        return True

    elif winner == "GREEN":
        screen.fill((0, 255, 0))
        return True

    return False


main()