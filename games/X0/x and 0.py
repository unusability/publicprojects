def main():
    end = True
    board = get_board()
    current_player = "X"
    move_count = 1
    while end:
        print_board(board)
        move_player = input(f"Ходит {current_player}. Введите поле от 1 до 9, 0 - Выход ")
        current_player = move(board, int(move_player), current_player, move_count)
        whowon(board)
        move_count +=1


def get_board():
    board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    return board

def print_board(board):
    print("+-------+-------+-------+")
    index = 0
    for x in range(3):
        print("|", end="")
        for y in range(3):
            print(f"   {board[index]}   |", end="")
            index += 1
        print()
        print("+-------+-------+-------+")

def move(board, move, current_player, move_count):
    if move_count < 9:
        if move != 0:
            if board[move - 1] == "X" or board[move - 1] == "O":
                print("Клетка занята, укажите другую")
            else:
                board[move - 1] = current_player
                if current_player == "X":
                    current_player = "O"
                else:
                    current_player = "X"
            return current_player
        elif move >= 10 or move < 0:
            print("Клетки не существует, укажите клетку от 1 до 9")
            return
        elif move == 0:
            print("GAME OVER")
            exit()
    else:
        print("Игра окончена, ничья!")
        exit()



def whowon(board):
    win_combination = (
        (0, 1, 2), (3, 4, 5), (6, 7, 8),
        (0, 3, 6), (1, 4, 7), (2, 5, 8),
        (0, 4, 8), (2, 4, 6)
    )
    for pos in win_combination:
        if board[pos[0]] == board[pos[1]] and board[pos[1]] == board[pos[2]]:
            print_board(board)
            print(f"{board[pos[0]]} победил!")
            exit()
    return
main()