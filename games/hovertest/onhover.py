import sys
import pygame

# Configuration
pygame.init()
fps = 60
fpsClock = pygame.time.Clock()
width, height = 110, 70
screen = pygame.display.set_mode((width, height))

# Before Main loop
rect = pygame.Rect(5, 5, 100, 60)
print(rect.width)
btn_surface = pygame.Surface((rect.width, rect.height))

normal_color = (200, 100, 100)
hover_color = (100, 200, 100)

# Game loop.
while True:
    screen.fill((20, 20, 20))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()

    # In the Main loop
    if rect.collidepoint(pygame.mouse.get_pos()):
        btn_surface.fill(hover_color)
    else:
        btn_surface.fill(normal_color)

    screen.blit(btn_surface, rect)

    pygame.display.flip()
    fpsClock.tick(fps)