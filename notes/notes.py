from tkinter import *

def answer():
    text.insert(1.0,'Я подготовил ответ на Ваше обращение:')
    text.focus_set()
def clear():
    text.delete(1.0,END)
root = Tk()
root.title('Notes')
frame1=Frame(root,bd=5)
frame2=Frame(root,bd=5)
frame1.pack()
frame2.pack(side = 'left')
text = Text(frame1, height=30, width=60, font = 'Sans 14')
text.pack(side = 'left')
scrollbar = Scrollbar(frame1)
scrollbar.pack(side = 'right',fill = "y")
btn = Button(frame2, text = 'Я подготовил ответ на Ваше обращение:',command = answer, font = 'Sans 14')
btn.pack(side = 'left', ipadx = 30, ipady = 8, padx = 10)

btn1 = Button(frame2, text = 'Очистить',command = clear, font = 'Sans 14')
btn1.pack(side = 'right', ipadx = 30, ipady = 8, padx = 30)
# первая привязка
scrollbar['command'] = text.yview
# вторая привязка
text['yscrollcommand'] = scrollbar.set
root.mainloop()